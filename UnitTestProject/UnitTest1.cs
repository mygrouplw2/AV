﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Mynamespace
{
    [TestClass]
    public class UnitTestList
    {
        [TestMethod]
        public void TestRemove()
        {
            string a="";
            DoublyLinkedList<string> linkedList = new DoublyLinkedList<string>();
            linkedList.Add("Bob");
            linkedList.Add("Bill");
            linkedList.Remove("Bill");
            foreach (var t in linkedList.BackEnumerator())
            {
                a=t;
            }
            Assert.AreEqual(a, "Bob");
        }
        [TestMethod]
        public void TestAdd()
        {
            string a = "";
            DoublyLinkedList<string> linkedList = new DoublyLinkedList<string>();
            linkedList.Add("Bob");
            foreach (var item in linkedList)
            {
                a = item;
            }
            Assert.AreEqual(a, "Bob");
        }
        [TestMethod]
        public void TestAddFirst()
        {
            string a = "";
            DoublyLinkedList<string> linkedList = new DoublyLinkedList<string>();
            linkedList.Add("Bob");
            linkedList.AddFirst("Kate");
            foreach (var item in linkedList)
            {
                a=item;
            }
            Assert.AreEqual(a, "Bob");
        }
        [TestMethod]
        public void TestClear()
        {
            DoublyLinkedList<string> linkedList = new DoublyLinkedList<string>();
            linkedList.Clear();
            Assert.AreEqual(0, linkedList.Count);
        }
        [TestMethod]
        public void TestBackEnumerator()
        {
            Assert.AreEqual(0, 0);
        }
        [TestMethod]
        public void TestClone()
        {
            Assert.AreEqual(0, 0);
        }

    }
    [TestClass]
    public class UnitTestStack
    {
        [TestMethod]
        public void TestPush()
        {
            FixedStack<string> stack = new FixedStack<string>(8);
            stack.Push("Kate");
            Assert.AreEqual(false, stack.IsEmpty);
        }
        [TestMethod]
        public void TestTPop()
        {
            FixedStack<string> stack = new FixedStack<string>(8);
            stack.Push("Kate");
            stack.Push("Sam");
            string head = stack.Pop().ToString();
            Assert.AreEqual(head, "Sam");
        }
        [TestMethod]
        public void TestTPeek()
        {
            FixedStack<string> stack = new FixedStack<string>(8);
            stack.Push("Kate");
            stack.Push("Sam");
            
            string head = stack.Peek().ToString();
            Assert.AreEqual(head, "Sam");
        }
    }
}
